﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WhatsAppSpammer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /*
        * This part of the code initiates the spam
        */

       private void startBtn_Click(object sender, EventArgs e)
       {
           int count;
           count = Convert.ToInt32(countText.Text);
           string msg;
           msg = msgText.Text;
           System.Threading.Thread.Sleep(10000);
           for (int i = 0; i < count; i++)
           {
               SendKeys.Send(msg+"{ENTER}");
           }
       }

       /*
        * This part of the code allows the user to drag the window
        */

        bool drag = false;
        Point initPoint;
        
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            drag = true;
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if(drag)
            {
                // Get the difference between the two points
                int xDiff = initPoint.X - e.Location.X;
                int yDiff = initPoint.Y - e.Location.Y;

                // Set the new point
                int x = this.Location.X - xDiff;
                int y = this.Location.Y - yDiff;
                this.Location = new Point(x, y);
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            drag = false;
        }

        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
