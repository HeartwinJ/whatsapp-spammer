﻿namespace WhatsAppSpammer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.titleLbl = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.msgText = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.msgLbl = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.countLbl = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.countText = new WindowsFormsControlLibrary1.BunifuCustomTextbox();
            this.startBtn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuImageButton1 = new Bunifu.Framework.UI.BunifuImageButton();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 5;
            this.bunifuElipse1.TargetControl = this;
            // 
            // titleLbl
            // 
            this.titleLbl.AutoSize = true;
            this.titleLbl.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(211)))), ((int)(((byte)(102)))));
            this.titleLbl.Location = new System.Drawing.Point(74, 9);
            this.titleLbl.Name = "titleLbl";
            this.titleLbl.Size = new System.Drawing.Size(338, 39);
            this.titleLbl.TabIndex = 0;
            this.titleLbl.Text = "WhatsApp Spammer";
            // 
            // msgText
            // 
            this.msgText.BorderColor = System.Drawing.Color.SeaGreen;
            this.msgText.Location = new System.Drawing.Point(227, 124);
            this.msgText.Multiline = true;
            this.msgText.Name = "msgText";
            this.msgText.Size = new System.Drawing.Size(200, 100);
            this.msgText.TabIndex = 1;
            // 
            // msgLbl
            // 
            this.msgLbl.AutoSize = true;
            this.msgLbl.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msgLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(211)))), ((int)(((byte)(102)))));
            this.msgLbl.Location = new System.Drawing.Point(21, 157);
            this.msgLbl.Name = "msgLbl";
            this.msgLbl.Size = new System.Drawing.Size(200, 30);
            this.msgLbl.TabIndex = 2;
            this.msgLbl.Text = "Enter Message :";
            // 
            // countLbl
            // 
            this.countLbl.AutoSize = true;
            this.countLbl.Font = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.countLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(211)))), ((int)(((byte)(102)))));
            this.countLbl.Location = new System.Drawing.Point(54, 294);
            this.countLbl.Name = "countLbl";
            this.countLbl.Size = new System.Drawing.Size(167, 30);
            this.countLbl.TabIndex = 3;
            this.countLbl.Text = "Enter Count :";
            // 
            // countText
            // 
            this.countText.BorderColor = System.Drawing.Color.SeaGreen;
            this.countText.Location = new System.Drawing.Point(227, 294);
            this.countText.Multiline = true;
            this.countText.Name = "countText";
            this.countText.Size = new System.Drawing.Size(100, 30);
            this.countText.TabIndex = 4;
            // 
            // startBtn
            // 
            this.startBtn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(211)))), ((int)(((byte)(102)))));
            this.startBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(211)))), ((int)(((byte)(102)))));
            this.startBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.startBtn.BorderRadius = 0;
            this.startBtn.ButtonText = "Start";
            this.startBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.startBtn.DisabledColor = System.Drawing.Color.Gray;
            this.startBtn.Iconcolor = System.Drawing.Color.Transparent;
            this.startBtn.Iconimage = null;
            this.startBtn.Iconimage_right = null;
            this.startBtn.Iconimage_right_Selected = null;
            this.startBtn.Iconimage_Selected = null;
            this.startBtn.IconMarginLeft = 0;
            this.startBtn.IconMarginRight = 0;
            this.startBtn.IconRightVisible = true;
            this.startBtn.IconRightZoom = 0D;
            this.startBtn.IconVisible = true;
            this.startBtn.IconZoom = 90D;
            this.startBtn.IsTab = false;
            this.startBtn.Location = new System.Drawing.Point(94, 395);
            this.startBtn.Margin = new System.Windows.Forms.Padding(7);
            this.startBtn.Name = "startBtn";
            this.startBtn.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(211)))), ((int)(((byte)(102)))));
            this.startBtn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(158)))), ((int)(((byte)(77)))));
            this.startBtn.OnHoverTextColor = System.Drawing.Color.White;
            this.startBtn.selected = false;
            this.startBtn.Size = new System.Drawing.Size(297, 47);
            this.startBtn.TabIndex = 5;
            this.startBtn.Text = "Start";
            this.startBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.startBtn.Textcolor = System.Drawing.Color.White;
            this.startBtn.TextFont = new System.Drawing.Font("Century Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startBtn.Click += new System.EventHandler(this.startBtn_Click);
            // 
            // bunifuImageButton1
            // 
            this.bunifuImageButton1.BackColor = System.Drawing.Color.White;
            this.bunifuImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.Image")));
            this.bunifuImageButton1.ImageActive = null;
            this.bunifuImageButton1.Location = new System.Drawing.Point(444, 12);
            this.bunifuImageButton1.Name = "bunifuImageButton1";
            this.bunifuImageButton1.Size = new System.Drawing.Size(24, 21);
            this.bunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton1.TabIndex = 6;
            this.bunifuImageButton1.TabStop = false;
            this.bunifuImageButton1.Zoom = 10;
            this.bunifuImageButton1.Click += new System.EventHandler(this.bunifuImageButton1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(480, 480);
            this.Controls.Add(this.bunifuImageButton1);
            this.Controls.Add(this.startBtn);
            this.Controls.Add(this.countText);
            this.Controls.Add(this.countLbl);
            this.Controls.Add(this.msgLbl);
            this.Controls.Add(this.msgText);
            this.Controls.Add(this.titleLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Form1";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private Bunifu.Framework.UI.BunifuCustomLabel msgLbl;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox msgText;
        private Bunifu.Framework.UI.BunifuCustomLabel titleLbl;
        private Bunifu.Framework.UI.BunifuFlatButton startBtn;
        private WindowsFormsControlLibrary1.BunifuCustomTextbox countText;
        private Bunifu.Framework.UI.BunifuCustomLabel countLbl;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton1;
    }
}

